
#capitalize -> Pone la primera letra en mayúscula
#nombre = input('nombre: ').capitalize()
#print('su nombre es', nombre)
#Castear/convertir
#tipo_dato( lo_que_se_convertirá )
#int('10')
#float('3.14')
#str()

""" num1 = float(input('Ingrese un número: '))
num2 = float(input('Ingrese otro número: '))
suma = num1 + num2
print('La suma es: ', suma) """


'''
--------------CALCULADORA-----------
1) Sumar
2) Restar
3) Multiplicar
4) Dividir
5) Salir
>>> 
'''

from os import system


def solicitar_numeros():
    n1 = float(input('Ingrese el primer número: '))
    n2 = float(input('Ingrese el segundo número: '))
    return {
        'n1': n1,
        'n2': n2
    }

def menu():
    mensaje_menu = '--------------CALCULADORA-----------\n'
    mensaje_menu += '1) Sumar\n'
    mensaje_menu += '2) Restar\n'
    mensaje_menu += '3) Multiplicar\n'
    mensaje_menu += '4) Dividir\n'
    mensaje_menu += '5) Salir\n'
    mensaje_menu += '>>> '

    opcion = ''
    while opcion != 5:
        #Obtener el dato/opción ingresada por el usuario y convertirla a entero
        opcion = int(input(mensaje_menu))
        #Evaluar la opción del usuario
        if opcion == 1:
            numeros = solicitar_numeros()
            suma = numeros['n1'] + numeros['n2']
            print(f'\nLa suma es :{suma}\n')
        elif opcion == 2:
            numeros = solicitar_numeros()
            resta = numeros["n1"] - numeros["n2"]
            print(f"\nLa resta es: {resta}\n")
        elif opcion == 3:
            print("Multiplicar")
            numeros = solicitar_numeros()
            multiplicacion=numeros["n1"] * numeros["n2"]
            print(f"\nLa multiplicacion es: {multiplicacion}")
        elif opcion == 4:
            print("Dividir")
            numeros = solicitar_numeros()
            division=numeros["n1"] / numeros["n2"]
            print(f"\nLa division es: {division}")
        elif opcion != 5:
            print('\nIngrese una opción válida\n')

menu()

