
contador = 0

while contador < 2000:
    print(contador)
    contador += 1

print("-----------------------")

contador = 0
while True:
    contador += 1
    print(contador)
    if contador == 200000:
        #break -> rompe el ciclo
        break

print("Fin del while")
